import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { User } from './auth/entity/user.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '103.150.116.13',
      port: 5432,
      username: 'hankamrata',
      password: 'hankamrata',
      database: 'hankamrata',
      entities: [User],
      synchronize: false,
      logging: true,
    }),
    ConfigModule.forRoot(),
    AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
