import { HttpException, Injectable } from '@nestjs/common'
import * as jwt from 'jsonwebtoken'

export const successResponse = (data = {}, message = 'success', statusCode = 200) => {
  return {
    success: true,
    message: message,
    statusCode: statusCode,
    data: data,
  }
}

export const errorResponse = (message = 'internal server error', statusCode = 500) => {
  throw new HttpException({
    success: false,
    statusCode: statusCode,
    message: message,
  }, statusCode)
}

export function currencyFormat(number, currency = 'IDR', countryCode = 'ID') {
  if (isNaN(number)) {
    return '0'
  }

  const result = new Intl.NumberFormat(countryCode, {
    style: 'currency', currency: currency, minimumFractionDigits: 0
  }).format(number)

  return result
}

export function upperWords(words) {
  return words.replace(/\b\w/g, c => c.toUpperCase())
}

@Injectable()
export class JWTUtil {

    verify(auth: string, secret_key: string): boolean{
        const token = auth.replace('Bearer ', '');
        jwt.verify(token, secret_key)
        try{
          jwt.verify(token, 'H@nk4mrata');
          return true;
        } catch(error){
          return false;
        }
    }
}