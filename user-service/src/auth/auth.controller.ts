import { Body, Controller, Headers, Post, Get, All, Request } from '@nestjs/common';
import { errorResponse, successResponse } from 'src/helper/helper';
import { AuthService } from './auth.service';
import { SignInDto, SignUpDto, SignUpNewDto, VerifyDto } from './dto/auth.dto';
import * as jwt from 'jsonwebtoken';
import fs from 'fs';
//import { exec } from "node:child_process";
import { exec } from 'shelljs';

@Controller('auth')
export class AuthController {
    constructor(
          private readonly authService: AuthService
        ) {}

        @Post('/signin')
        async create(@Body() dto: SignInDto, @Request() req): Promise<any> {
          try {
            const data = await this.authService.signin(dto)
            return successResponse({ token: data }, 'Signin success')
          } catch (error) {
            return errorResponse(error.message)
          }
        }

        @Post('/register')
        async register(@Body() dto: SignUpNewDto, @Request() req): Promise<any> {
          const path = '../configurations/ejabberd_conf.py';
          const domain = 'talk.hankamrata.id';

          try {
            let user_data = await this.authService.viewUser(dto.nrp)
            if(user_data){
              //console.log(user_data);
              const data = await this.authService.update(dto);
              if (fs.existsSync(path)) {
                exec(path+" changepassword "+dto.nrp+" "+domain+" "+(dto.device_key.slice(10)), (error, stdout, stderr) => {
                  console.log("STDOUT:", stdout, ", STDERR:", stderr);
                });
              }
              return successResponse({ user: data }, 'Registration update success')
            } else {
              const data = await this.authService.signup(dto)
              if (fs.existsSync(path)) {
                // File exists in path
                exec(path+" register "+dto.nrp+" "+domain+" "+(dto.device_key.slice(10)), (error, stdout, stderr) => {
                    console.log("STDOUT:", stdout, ", STDERR:", stderr);
                  });
              }
              return successResponse({ user: data }, 'Register user success')
            }

            // const registercmd = "docker exec -i ejabberd ejabberdctl register "+dto.nrp+
            //                     " talk.hankamrata.id "+password;
            // console.log(registercmd);
            // const child = exec(registercmd)
            
          } catch (error) {
            return errorResponse(error.message)
          }
        }

        @All('/verify')
        async verify(@Body() dto: VerifyDto, @Headers('Authorization') auth: string): Promise<any> {
          try {
            const secret_key = 'H@nk4mrata';
            let token = '';
            if(auth!==undefined){
              if(auth.startsWith('Bearer')){
                token = auth.replace('Bearer ', '');
              }else{
                return errorResponse('Invalid Authorization header');
              }
            }else if(dto.token!==''){
              token = dto.token;
            }else{
              return errorResponse("No authorization token provided");
            }
            let decoded = jwt.verify(token, secret_key);
            return successResponse(decoded, 'Token is valid');
          } catch (error) {
            return errorResponse(error.message)
          }
        }
}
