import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import axios from 'axios';
import { SignInDto, SignUpDto, SignUpNewDto, VerifyDto } from './dto/auth.dto';
import { User } from './entity/user.entity';
import * as jwt from 'jsonwebtoken'
import { sha256 } from 'js-sha256';
import {v4 as uuidv4} from 'uuid';

@Injectable()
export class AuthService {
    private readonly SECRET_KEY = 'H@nk4mrata';

    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
      ) {}
    
    findAllUser(): Promise<User[]> {
        return this.userRepository.find();
      }

    viewUser(user_nrp: string): Promise<User> {
        let usr = this.userRepository.findOneBy({ user_nrp: user_nrp });
        console.log(usr);
        return usr;
      }

    // async insert(userDetails: SignUpDto): Promise<User> {
    //     const user: User = new User(); // = this.userRepository.create()
    //     const {id, password, fullname, mobileno, email } = userDetails;
    //     user.user_id = uuidv4();
    //     user.user_nrp = id;
    //     user.user_passhash = sha256(password);
    //     user.user_fullname = fullname;
    //     user.user_email = email;
    //     user.user_mobileno = mobileno;
    //     user.satdet_kode = "KORAMIL0502/01";
    //     user.user_status = "AKTIF";
        
    //     console.log(user);

    //     return this.userRepository.save(user)
    //   }

    async insert(userDetails: SignUpNewDto): Promise<User> {
      const user: User = new User(); // = this.userRepository.create()
      const {nrp, full_name, unit, device_id, device_key } = userDetails;
      user.user_id = uuidv4();
      user.user_nrp = nrp;
      user.user_passhash = device_key;
      user.user_fullname = full_name;
      user.user_email = nrp+"@talk.hankamrata.id";
      user.user_mobileno = device_id;
      user.satdet_kode = unit;
      user.user_status = "AKTIF";
      
      console.log(user);

      return this.userRepository.save(user)
    }

    async update(userDetails: SignUpNewDto): Promise<User> {
      const {nrp, full_name, unit, device_id, device_key } = userDetails;
      try{
        let existing_user = await this.viewUser(nrp)
        if(existing_user){
          const user: User = new User();
          user.user_id = existing_user.user_id;
          user.user_nrp = nrp;
          user.user_passhash = device_key;
          user.user_fullname = full_name;
          user.user_email = nrp+"@im.hankamrata.id";
          user.user_mobileno = device_id;
          user.satdet_kode = unit;
          user.user_status = "AKTIF";
          console.log(user);
          await this.userRepository.update(user.user_id, user);
          return user;
        }
      } catch(e){
        return null;
      }
    }

    async signin(dto: SignInDto) {
        let usr = this.viewUser(dto.username);
        //'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f' = 12345678
        let passhash = dto.password; //sha256(dto.password)
        if((await usr).user_passhash==passhash){
            
            const token = jwt.sign({ _id: (await usr).user_id, name: (await usr).user_nrp }, this.SECRET_KEY, {
                expiresIn: '2 days',
              });
            return token;
        } else {
          throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
        }
    }

    async verify(dto: VerifyDto) {
      try{
        jwt.verify(dto.token, this.SECRET_KEY);
      }catch(error){
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      }
    }

    async signup(dto: SignUpNewDto) {
      // const configURL = 'http://api.hankamrata.id/'
      // let data = await axios.post(configURL,{
      //       username: dto.id,
      //       password: dto.password
      //   });
        
      return await this.insert(dto);
    }
}