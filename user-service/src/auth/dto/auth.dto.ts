import { ApiProperty } from "@nestjs/swagger"
import { IsEmail, IsNotEmpty } from 'class-validator';

export class SignInDto {
  @ApiProperty()
  username: string

  @ApiProperty()
  password?: string
}

export class SignUpDto {
  @ApiProperty()
  id?: string

  @ApiProperty()
  password?: string

  @ApiProperty()
  fullname?: string

  @ApiProperty()
  mobileno?: string

  @ApiProperty()
  email?: string
}

export class SignUpNewDto {
  @ApiProperty()
  nrp?: string

  @ApiProperty()
  full_name?: string

  @ApiProperty()
  unit?: string

  @ApiProperty()
  device_id?: string

  @ApiProperty()
  device_key?: string
}

export class VerifyDto {
  @IsNotEmpty()
  @ApiProperty()
  token: string
}