import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class User {
  /**
   * this decorator will help to auto generate id for the table.
   */
  //@PrimaryColumn({ type: 'uuid' })
  @PrimaryGeneratedColumn('uuid')
  user_id: string;

  @Column({ type: 'varchar', nullable: true })
  user_nrp: string;

  @Column({ type: 'varchar', nullable: true })
  user_fullname: string;

  @Column({ type: 'varchar', nullable: true })
  user_passhash: string;

  @Column({ type: 'varchar', nullable: true })
  user_email: string;

  @Column({ type: 'varchar', nullable: true })
  user_mobileno: string;

  @Column({ type: 'varchar', nullable: true })
  satdet_kode: string;

  @Column({ type: 'timestamp', nullable: true })
  user_lastlogin: string;

  //@Column({ type: 'enum', enum: ['ACTIVE', 'INACTIVE'], nullable: false })
  @Column({ type: 'varchar', nullable: true })
  user_status: string;
}