# A Modified Fake API JWT JSON Server

This is a fake API JWT JSON that will be use for Traefik ForwardAuth demo

Credits: techiediaries@github
Forked from: https://github.com/techiediaries/fake-api-jwt-json-server

## Test

### Test 1: Access whoami service without auth middleware, using Traefik routing host 'whoami.docker.localhost'
```
$ curl http://localhost/whoami
```

### Test 2: Access products endpoint 

Access product endpoint without auth token, to prove that the actual products endpoint is not protected with auth:
```
$ curl -H 'Content-Type: application/json' http://localhost/api/unsecured/products
```
Result: Success

Now, try it again via Traefik host routing without token:
```
$ curl -H 'Content-Type: application/json' http://localhost/api/secured/products
```
Result: Failes. 401 "Error in authorization format"

### Test 3: Login to get Token
```
$ curl -H 'Content-Type: application/json' -X POST http://localhost/auth/login -d '{"email": "nilson@email.com", "password": "nilson"}'
```
Result: Success, with token data

Or you can also use the exposed port of auth service, like this:
```
$ curl -H 'Content-Type: application/json' -X POST http://localhost:8000/auth/login -d '{"email": "nilson@email.com", "password": "nilson"}'
```
Result: Success, with token data

Output Example:
```
{
   "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y"
}
```

### Test 4: Access products endpoint again, with token, via Traefik host routing
```
$ curl -H 'Host: api.docker.localhost' -H 'Content-Type: application/json' -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y' http://localhost/products 
```
Result: Success, with product list

You can try other endpoints:
```
curl -H 'Host: api.docker.localhost' -H 'Content-Type: application/json' -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y' http://localhost/transactions 
```
```
curl -H 'Host: whoamiauth.docker.localhost' -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y' http://localhost

```
Or try it with wrong token, to make sure that the forwardauth is working well:
```
$ curl -H 'Host: api.docker.localhost' -H 'Content-Type: application/json' http://localhost/transactions -H 'Authorization: Bearer WRONG_TOKEN_HERE'
```

# JSONServer + JWT Auth

A Fake REST API using json-server with JWT authentication. 

Implemented End-points: login,register

## Install

```bash
$ npm install
$ npm run start-auth
```

Might need to run
```
npm audit fix
```

## How to login/register?

You can login/register by sending a POST request to

```
POST http://localhost:8000/auth/login
POST http://localhost:8000/auth/register
GET http://localhost:8000/verifytoken
GET http://localhost:8000/products
GET http://localhost:8000/location
GET http://localhost:8000/verifytoken
```
with the following data 

```
{
  "email": "nilson@email.com",
  "password":"nilson"
}
```

Example:
```
curl -H 'Content-Type: application/json' -X POST http://localhost:8000/auth/login -d '{"email": "nilson@email.com", "password": "nilson"}'
```

You should receive an access token. Example:

```
{
   "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y"
}
```

You should send this authorization with any request to the protected endpoints

```
Authorization: Bearer <ACCESS_TOKEN>
```

Verify token example:
```
curl -H 'Content-Type: application/json' http://localhost:8000/verifytoken -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y'
```

Or, access products endpoint:
```
curl -H 'Content-Type: application/json' http://localhost:8000/products -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y'
```

Or, transactions endpoint:
```
curl -H 'Content-Type: application/json' http://localhost:8000/transactionss -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y'
```

or, whoamiauth endpoint:
```
curl -H 'Host: whoamiauth.docker.localhost' -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5pbHNvbkBlbWFpbC5jb20iLCJwYXNzd29yZCI6Im5pbHNvbiIsImlhdCI6MTY0MTM0MTAwM30.lSKdmNYhp7C3xMMmCZXFOvt1lkYX2EnCBoD9Ab7g7-Y' http://localhost/
```
